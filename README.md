# Adventures of Tintin and Haddock
A two player game developed in **pygame** in which the objective of the players is to cross to the other side.

## Components of the game:
1. Player 1 - Tinitn
2. Player 2 - Captain Haddock
3. Stationary obstacle - Manhole
4. Moving obstacle - Madam Castafiori

## How to play:
### Controls
Players use the arrow keys to navvigate through the screen.
Press "q" at any time you want to know who is winning.

### AIM
The goal of the game is to get from "START" to "FINISH".

### Point system
On crossing the stationay obstacles, 5 points are awarded to the respective player and 10 points on crossing a moving obstacle.
If there is more than one stationay obstacle in a given layer, then the total number of obstacles will be considered.
In case of a draw, the player with the least total time taken while active wins.

### Upon collision
The score is updated and control is transferred to the other player.

### Ending the game
This is an infinite level game. If you wish to exit at any point, press "q" to view the winner, press "q" again and close the screen.
