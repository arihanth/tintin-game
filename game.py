# imports libraries, files
import pygame
from pygame import time
import sys
import os
import random
import configparser
from pygame import mixer
import struct

# defining the config file
cf = configparser.RawConfigParser()
configfile = "./config.cfg"
cf.read(configfile)

# values from config file
WHITE = bytes.fromhex(cf.get("colors", "white"))
font = cf.get("font", "font")
p1_mess = cf.get("mess_p1", "mess_p1")
p2_mess = cf.get("mess_p2", "mess_p2")
ground_img = cf.get("img", "ground")
grass_img = cf.get("img", "grass")
player_vel = int(cf.get("rest", "player_vel"))
fiori_vel = int(cf.get("rest", "fiori_vel"))
height = int(cf.get("rest", "height"))
width = int(cf.get("rest", "width"))
start_mess = cf.get("rest", "start")
finish_mess = cf.get("rest", "finish")
hit_mhole = cf.get("rest", "hit_mhole")
hit_fiori = cf.get("rest", "hit_fiori")

# sets size, usefull information
pygame.init()
clock = pygame.time.Clock()
canvas = pygame.display.set_mode((width, height))
add_y = 231.25
count = [0]
success = [1]
layer = []
change = []
text = pygame.font.SysFont(font, 12)
bold = pygame.font.SysFont(font, 24)
cur_time = []
cur_time.append(0)
for i in range(0, 9):
    change.append(1)


# defines background
class Background:
    def __init__(self, x_size, y_size, bg_img):
        self.x_size = x_size
        self.y_size = y_size
        self.bg_img = pygame.image.load(bg_img)


# creates background
def background():
    x = 0
    y = 75
    for i in range(0, 5):
        ground = pygame.transform.scale(pygame.image.load(
                                        ground_img), (70, 125 + 31))
        for k in range(0, 10):
            canvas.blit(ground, (x, y))
            x = x + 70
        x = 0
        y = y + add_y

    x = y = 0
    for i in range(0, 5):
        grass = pygame.transform.scale(pygame.image.load(
                                        grass_img), (70, 75))
        for k in range(0, 10):
            canvas.blit(grass, (x, y))
            x = x + 70
        x = 0
        y = y + add_y

    x = 0
    y = 150 + add_y
    for i in range(0, 2):
        tiny_grass = pygame.transform.scale(pygame.image.load(
                                            grass_img), (70, 10))
        for k in range(0, 10):
            canvas.blit(tiny_grass, (x, y))
            x = x + 70
        x = 0
        y = y + add_y


# defines player
class Player:
    def __init__(self, x, y, img, lvl):
        self.x = x
        self.y = y
        self.img = pygame.transform.scale(pygame.image.load(
                    img), (75, 75))
        self.lvl = lvl
        self.speed = player_vel
        self.rect = self.img.get_rect()
        self.score = 0
        self.time = 0


p_1 = Player(315, 925, "./img/tintin_snowy_rotated.png", 1)
p_2 = Player(315, 0, "./img/haddock_angry.gif", 1)
current_player = []
current_player.append(p_1)  # sets the current player


# defines any obstacle
class obstacle:
    def __init__(self, img, lvl, alt):
        self.img = pygame.transform.scale(pygame.image.load(
                    img), (75, 75))
        self.alt = pygame.transform.scale(pygame.image.load(
                    alt), (75, 75))
        self.lvl = lvl
        self.rect = self.img.get_rect()


# contains details of all manholes
class Manhole():
    def __init__(self, img, x, y):
        self.img = pygame.transform.scale(pygame.image.load(
                    img), (75, 75))
        self.x = x
        self.y = y
        self.rect = self.img.get_rect()


# creates "fiori" - moving obstacle
class CastaFiori():
    def __init__(self, x, y):
        self.pos = current_player[0].lvl * 5
        self.x = x
        self.y = y
        random.seed(current_player[0].lvl + 1000000006)
        self.x1 = random.randrange(50, 200, 50)
        self.x2 = random.randrange(500, 650, 50)


# moves player
def move_player(p, xdist, ydist):
    p.x = p.x + xdist
    p.y = p.y + ydist
    if p.x < 0:
        p.x = 0
    if p.x > 625:
        p.x = 625
    if p.y < 0:
        p.y = 0
    if p.y > 925:
        p.y = 925


# set obstacles
manhole = obstacle("./img/manhole.png", 1, "./img/manhole.png")
fiori = obstacle("./img/castafiori.png", 1, "./img/castafiori_flipped.png")


# defining list of objects
manhole_list = []
fiori_list = []


# creates "man holes" - stationary obstacle
def man_hole():
    k = 0
    random.seed(current_player[0].lvl + 1000000006)
    for j in range(1, 4):
        t = random.randrange(1, 6, 1)
        for i in range(0, t):
            x = random.randrange(0, 700, 100)
            k = k + 1
            if count[0] < k:
                count[0] = k
                store_manhole(x, add_y * j)
            canvas.blit(manhole.img, (x, add_y * j))
        layer.append(t)


# contains the coordinates of the manholes
def store_manhole(x, y):
    manhole_list.append(Manhole("./img/manhole.png", x, y))


# defining initial fiori
mov_fiori = CastaFiori(0, 0)  # creating object of class "CastaFiori"


# moves "fiori" - moving obstacle
def moving_fiori():
    mov_fiori.pos = current_player[0].lvl * fiori_vel
    random.seed(current_player[0].lvl + 1000000006)

    if mov_fiori.x1 >= 700:
        mov_fiori.x1 = -75
    if mov_fiori.x2 <= -75:
        mov_fiori.x2 = 700

    canvas.blit(fiori.alt, (mov_fiori.x1, 110))
    fiori_list.append(CastaFiori(mov_fiori.x1, 110))

    canvas.blit(fiori.img, (mov_fiori.x2, 73+add_y))
    fiori_list.append(CastaFiori(mov_fiori.x2, 73+add_y))

    canvas.blit(fiori.alt, (mov_fiori.x1, 73+add_y+85))
    fiori_list.append(CastaFiori(mov_fiori.x1, 73+add_y+85))

    canvas.blit(fiori.img, (mov_fiori.x2, 1000-(73+add_y+85)-75))
    fiori_list.append(CastaFiori(mov_fiori.x2, 1000-(73+add_y+85)-75))

    canvas.blit(fiori.alt, (mov_fiori.x1, 1000-(73+add_y)-75))
    fiori_list.append(CastaFiori(mov_fiori.x1, 1000-(73+add_y)-75))

    canvas.blit(fiori.img, (mov_fiori.x2, 810))
    fiori_list.append(CastaFiori(mov_fiori.x2, 810))

    mov_fiori.x1 = mov_fiori.x1 + mov_fiori.pos
    mov_fiori.x2 = mov_fiori.x2 - mov_fiori.pos


# checking collision with manhole
def collision_manhole():
    for k in range(0, count[0]):
        if col_bw(current_player[0], manhole_list[k]):
            success[0] = 0
            print(hit_mhole)


# checking collision with fiori
def collision_fiori():
    for k in range(0, 6):
        if col_bw(current_player[0], fiori_list[k]):
            success[0] = 0
            print(hit_fiori)


# function to detect collision
def col_bw(a, b):
    if abs(a.x - b.x) <= 60 and abs(a.y - b.y) <= 60:
        return True
    else:
        return False


# resets the count and collision objects
def reset():
    count[0] = 0
    manhole_list.clear()
    fiori_list.clear()
    layer.clear()
    success[0] = 1
    for i in range(0, 9):
        change[i] = 1


# sets new player to p_1
def decide_player1():
    if success[0]:
        if current_player[0].y >= 925:
            current_player[0].lvl = current_player[0].lvl + 1
            current_player[0].x = 315
            current_player[0].y = 0
            current_player[0] = p_1
            reset()
    else:
        current_player[0].x = 315
        current_player[0].y = 0
        current_player[0] = p_1
        reset()
    p_2.time = cur_time[0] - p_1.time


# sets new player to p_2
def decide_player2():
    if success[0]:
        if current_player[0].y <= 0:
            current_player[0].lvl = current_player[0].lvl + 1
            current_player[0].x = 315
            current_player[0].y = 925
            current_player[0] = p_2
            reset()
    else:
        current_player[0].x = 315
        current_player[0].y = 925
        current_player[0] = p_2
        reset()
    p_1.time = cur_time[0] - p_2.time


# decides player and changes player's level
def maintain():
    if current_player[0] == p_1:
        decide_player2()
    elif current_player[0] == p_2:
        decide_player1()


# displays when p1 wins
def disp_score1():
    score1 = text.render("P1: " + str(p_1.score), True, WHITE)
    canvas.blit(score1, (10, 10))


# displays when p2 wins
def disp_score2():
    score2 = text.render("P2: " + str(p_2.score), True, WHITE)
    canvas.blit(score2, (10, 30))


# keeps score of p1
def keep_score_p1():
    p = current_player[0]
    if p == p_1:
        if p.y > 612 and p.y < 690 and change[0]:
            p.score = p.score + (5 * layer[2])
            change[0] = 0
        if p.y > 382 and p.y < 460 and change[1]:
            p.score = p.score + (5 * layer[1])
            change[1] = 0
        if p.y > 75 and p.y < 230 and change[2]:
            p.score = p.score + (5 * layer[0])
            change[2] = 0
        if p.y > 765 and p.y < 800 and change[3]:
            p.score = p.score + 10
            change[3] = 0
        if p.y > 535 and p.y < 612 and change[4]:
            p.score = p.score + 10
            change[4] = 0
        if p.y > 460 and p.y < 535 and change[5]:
            p.score = p.score + 10
            change[5] = 0
        if p.y > 305 and p.y < 382 and change[6]:
            p.score = p.score + 10
            change[6] = 0
        if p.y > 230 and p.y < 305 and change[7]:
            p.score = p.score + 10
            change[7] = 0
        if p.y > 75 and p.y < 110 and change[8]:
            p.score = p.score + 10
            change[8] = 0


# keep score of p2
def keep_score_p2():
    p = current_player[0]
    if p == p_2:
        if p.y > 230 and p.y < 305 and change[0]:
            p.score = p.score + (5 * layer[0])
            change[0] = 0
        if p.y > 460 and p.y < 540 and change[1]:
            p.score = p.score + (5 * layer[1])
            change[1] = 0
        if p.y > 690 and p.y < 850 and change[2]:
            p.score = p.score + (5 * layer[2])
            change[2] = 0
        if p.y > 115 and p.y < 160 and change[3]:
            p.score = p.score + 10
            change[3] = 0
        if p.y > 305 and p.y < 390 and change[4]:
            p.score = p.score + 10
            change[4] = 0
        if p.y > 385 and p.y < 460 and change[5]:
            p.score = p.score + 10
            change[5] = 0
        if p.y > 540 and p.y < 615 and change[6]:
            p.score = p.score + 10
            change[6] = 0
        if p.y > 615 and p.y < 690 and change[7]:
            p.score = p.score + 10
            change[7] = 0
        if p.y > 800 and p.y < 850 and change[8]:
            p.score = p.score + 10
            change[8] = 0


# displays start and finish lines
def disp_ends():
    start = text.render(start_mess, True, WHITE)
    finish = text.render(finish_mess, True, WHITE)
    if current_player[0] == p_2:
        canvas.blit(start, (600, 10))
        canvas.blit(finish, (600, 970))
    else:
        canvas.blit(finish, (600, 10))
        canvas.blit(start, (600, 970))


# display score when "q" is pressed
def check_score():
    amt = abs(p_1.score - p_2.score)
    tm = abs(p_1.time - p_2.time)
    if p_1.score > p_2.score:
        end = bold.render(p1_mess + " " + str(amt) + " points", True, WHITE)
    elif p_1.score < p_2.score:
        end = bold.render(p2_mess + " " + str(amt) + " points", True, WHITE)
    elif p_1.score == p_2.score:
        if p_1.time < p_2.time:
            end = bold.render(p1_mess + " " + str(tm) + " secs", True, WHITE)
        else:
            end = bold.render(p2_mess + " " + str(tm) + " secs", True, WHITE)
    canvas.blit(end, (250, 250))


# initializing varibales
move_x = move_y = 0
done = False
chk_score = False


while not done:
    cur_time[0] = pygame.time.get_ticks() // 1000
    # listens for events
    for event in pygame.event.get():
        # force shut the game - points are not displayed
        if event.type == pygame.QUIT:
            done = True

        # moves the sprite when key is pressed
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                move_x = -current_player[0].speed
            elif event.key == pygame.K_RIGHT:
                move_x = current_player[0].speed
            elif event.key == pygame.K_DOWN:
                move_y = current_player[0].speed
            elif event.key == pygame.K_UP:
                move_y = -current_player[0].speed

        # moves the sprite until key is released
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_RIGHT or event.key == pygame.K_LEFT:
                move_x = 0
            elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                move_y = 0

        # press q to end game
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                chk_score = True

    # displays score every time you press "q"
    while chk_score:
        check_score()
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    chk_score = False
                    break
        clock.tick(60)
        pygame.display.update()

    # calling set of functtions
    maintain()
    background()
    man_hole()
    moving_fiori()

    # moves the player around
    move_player(current_player[0], move_x, move_y)

    canvas.blit(current_player[0].img, (
                current_player[0].x, current_player[0].y))

    # calls for collision checking
    keep_score_p1()
    keep_score_p2()
    collision_manhole()
    collision_fiori()
    fiori_list.clear()
    disp_score1()
    disp_score2()
    disp_ends()

    # updates the screen
    clock.tick(60)
    pygame.display.update()
